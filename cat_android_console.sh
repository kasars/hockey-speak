#!/bin/sh

APP_PID=$(adb shell pidof org.sars.hockeyspeak)

adb logcat --pid $APP_PID
