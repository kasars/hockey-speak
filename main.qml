/*
 * SPDX-FileCopyrightText: 2021-2023 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtQuick.Window
import QtCore
import QtQuick.Controls as QC
import Qt.labs.platform
import QtQuick.Controls.Material
import sars.MobileHelpers
import "./" as Root


ApplicationWindow {
    id: hSpeak
    width: 480
    height: 800
    visible: true
    title: qsTr("Hockey Speak")


    readonly property color bgColor: helpers.systemIsDarkMode ? "black" : "white"
    color: bgColor
    MobileHelpers {
        id: helpers
        statusbarColor: hSpeak.bgColor
    }
    Material.theme: helpers.systemIsDarkMode ? Material.Dark : Material.Light

    readonly property int pageMargins: 10

    property string soundList: ""
    property string trackList: ""

    property bool editMode: false

    Image {
        anchors.fill: parent
        source: helpers.systemIsDarkMode ? "LogoDM.png" : "Logo.png"
        fillMode: Image.PreserveAspectFit
        opacity: 0.07
    }

    onEditModeChanged: listChangesDone()

    FocusScope {
        anchors.fill: parent
        focus: true

        GridView {
            id: effectsGrid
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                margins: pageMargins
                rightMargin: 0
            }
            readonly property int columns: editMode ? 2 : 3
            height: Math.min(Math.round(parent.height / 3), effectsGrid.contentHeight)

            cellWidth: Math.floor(effectsGrid.width / columns)
            cellHeight: pageMargins * 4

            clip: true
            interactive: effectsGrid.height < effectsGrid.contentHeight
            QC.ScrollBar.vertical: gridScrollbar

            model: soundListModel

            delegate: Item {
                id: btnDelegate
                width: Math.floor(effectsGrid.width / effectsGrid.columns - pageMargins)
                height: pageMargins * 3

                Root.SoundButton {
                    anchors.fill: parent
                    trackName: model.name
                    soundSource: model.track
                    soundVolume: model.volume
                    visible: !hSpeak.editMode
                }

                Root.StyledInput {
                    id: buttonInput
                    anchors {
                        left: parent.left
                        top: parent.top
                        bottom: parent.bottom
                        right: removeButton.left
                        rightMargin: pageMargins * 0.5
                    }
                    text: model.name
                    visible: hSpeak.editMode
                    onTextChanged: {
                        soundListModel.setProperty(model.index, "name", buttonInput.text);
                    }
                }

                Root.IconLabel {
                    id: removeButton
                    icon: helpers.systemIsDarkMode ? "QuitButtonDM.png" : "QuitButton.png"
                    anchors {
                        right: parent.right
                        top: parent.top
                        bottom: parent.bottom
                    }
                    width: height
                    onClicked: {
                        remover.removeAt(model.index, soundListModel);
                    }
                    visible: editMode
                }

            }
        }

        QC.ScrollBar {
            id: gridScrollbar
            anchors {
                right: parent.right
                top: effectsGrid.top
                bottom: effectsGrid.bottom
            }
            active: true
            policy: QC.ScrollBar.AlwaysOn
            visible: effectsGrid.interactive
            Material.theme: helpers.systemIsDarkMode ? Material.Dark : Material.Light
        }


        Component {
            id: soundItem
            Root.SoundPlayItem {
                width: tracksListView.width
                height: playing || modelIndex == tracksListView.currentIndex ? pageMargins * 6 : pageMargins * 3
                trackName: modelName
                soundSource: modelTrack
                soundVolume: modelVolume
                editMode: hSpeak.editMode

                Behavior on height {
                    NumberAnimation { duration: 200 }
                }

                onRemoveClicked: {
                    remover.removeAt(modelIndex, trackListModel);
                }
                onEndReached: {
                    if (modelIndex < tracksListView.count-1) {
                        tracksListView.itemAtIndex(modelIndex+1).loaderItem.play();
                    }
                }
                onClicked: {
                    tracksListView.currentIndex = modelIndex;
                }

                onTrackNameChanged: {
                    trackListModel.setProperty(modelIndex, "name", trackName);
                }
                onSoundVolumeChanged: {
                    trackListModel.setProperty(modelIndex, "volume", soundVolume);
                }
            }
        }

        ListView {
            id: tracksListView
            anchors {
                top: effectsGrid.bottom
                left: parent.left
                right: parent.right
                bottom: periodicItem.visible ? periodicItem.top :
                                               periodicConfig.visible ? periodicConfig.top :
                                               showPeriodicButton.top
                margins: pageMargins
                topMargin: 0 // The effectGrid already has pageMargins worth of extra space
                bottomMargin: periodicItem.visible ? 0 : pageMargins
            }
            clip: true
            interactive: tracksListView.height < tracksListView.contentHeight
            QC.ScrollBar.vertical: scrollbar
            spacing: pageMargins
            currentIndex: -1

            model: trackListModel

            delegate: Item {
                property alias loaderItem: itemLoader.item
                width: tracksListView.width
                height: itemLoader.item ? itemLoader.item.height : pageMargins * 3
                Rectangle {
                    anchors.fill: parent
                    color: helpers.systemIsDarkMode ? "#555555" : "#DDDDDD"
                    Text {
                        anchors.centerIn: parent
                        text: qsTr("Loading...")
                        color: helpers.systemIsDarkMode ? "#ffffff" : "#000000"
                    }
                    opacity: 0.9
                    visible: itemLoader.status !== Loader.Ready
                }
                Loader {
                    id: itemLoader
                    asynchronous: true
                    property string modelName: model.name
                    property string modelTrack: model.track
                    property double modelVolume: model.volume
                    property int modelIndex: model.index
                    sourceComponent: soundItem
                }
                MouseArea {
                    anchors.fill: parent
                    readonly property bool playing: itemLoader.item && itemLoader.item.playing ? true : false
                    visible: index !== tracksListView.currentIndex && !playing
                    onClicked: tracksListView.currentIndex = index
                }
            }
        }

        QC.ScrollBar {
            id: scrollbar
            anchors {
                right: parent.right
                top: tracksListView.top
                bottom: tracksListView.bottom
            }
            policy: QC.ScrollBar.AlwaysOn
            visible: tracksListView.interactive
            Material.theme: helpers.systemIsDarkMode ? Material.Dark : Material.Light
        }

        Timer {
            id: remover
            property int index: -1
            property var model;
            function removeAt(index, model) {
                remover.model = model;
                remover.index = index;
                remover.start();
            }
            interval: 1
            onTriggered: {
                if (remover.index >= 0 && remover.index < remover.model.count) {
                    remover.model.remove(remover.index, 1);
                    if (remover.model.count === 0) {
                        remover.model.setDefaults();
                    }
                }
                remover.index = -1;
                listChangesDone();
            }
        }

        Root.PeriodicTrack {
            id: periodicItem
            anchors {
                left: parent.left
                right: parent.right
                bottom: showPeriodicButton.top
                bottomMargin: pageMargins
            }
            visible: showPeriodicButton.showPeriodic
            portrait: hSpeak.height > hSpeak.width
            intervalTrackUrl: intervalCombo.currentValue
        }

        Row {
            id: periodicConfig
            anchors {
                left: parent.left
                right: parent.right
                bottom: showPeriodicButton.top
                margins: pageMargins
            }
            spacing: pageMargins
            visible: hSpeak.editMode

            Text {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Periodic Sound")
                color: helpers.systemIsDarkMode ? "#ffffff" : "#000000"
            }

            QC.ComboBox {
                id: intervalCombo
                model: soundListModel
                textRole: "name"
                valueRole: "track"
                currentIndex: trackToIndex(periodicItem.intervalTrackUrl.toString())
                Material.theme: helpers.systemIsDarkMode ? Material.Dark : Material.Light

                function trackToIndex(url: string) : int
                {
                    for (var i=0; i< soundListModel.count; ++i) {
                        if (url === soundListModel.get(i).track) {
                            return i;
                        }
                    }
                    return 0;
                }
            }
        }

        Row {
            anchors {
                left: parent.left
                bottom: parent.bottom
                margins: pageMargins
            }
            spacing: pageMargins

            Root.Button {
                id: eritButton
                text: hSpeak.editMode ? qsTr("Done") : qsTr("Edit")
                onClicked: {
                    showPeriodicButton.showPeriodic = false;
                    hSpeak.editMode = !hSpeak.editMode;
                }
                disabled: periodicItem.playing
            }

            Root.Button {
                id: addSoundButton
                text: qsTr("Add Effect")
                onClicked: {
                    effectDialog.open();
                }
                visible: hSpeak.editMode
            }

            Root.Button {
                id: addTrackButton
                text: qsTr("Add Track")
                onClicked: {
                    trackDialog.open();
                }
                visible: hSpeak.editMode
            }

        }

        Root.Button {
            id: showPeriodicButton
            anchors {
                right: parent.right
                bottom: parent.bottom
                margins: pageMargins
            }
            width: Math.max(implicitWidth, hidePeriodicButton.implicitWidth)
            text: qsTr("Show Periodic Timer")
            property bool showPeriodic: false
            onClicked: {
                showPeriodic = true;
            }
            visible: !showPeriodicButton.showPeriodic && !hSpeak.editMode
        }

        Root.Button {
            id: hidePeriodicButton
            anchors {
                right: parent.right
                bottom: parent.bottom
                margins: pageMargins
            }
            width: Math.max(implicitWidth, showPeriodicButton.implicitWidth)
            text: qsTr("Hide Periodic Timer")
            onClicked: {
                showPeriodicButton.showPeriodic = false;
            }
            visible: showPeriodicButton.showPeriodic
        }

        FileDialog {
            id: effectDialog
            nameFilters: ["Ogg Vorbis files (*.ogg)", "MP3 files (*.mp3)"]
            folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
            fileMode: FileDialog.OpenFiles
            onAccepted: {
                for (var i=0; i<effectDialog.currentFiles.length; ++i) {
                    soundListModel.append({ name: "", track: ""+effectDialog.currentFiles[i], volume: 1.0 });
                }
                listChangesDone();
            }
        }

        FileDialog {
            id: trackDialog
            nameFilters: ["Ogg Vorbis files (*.ogg)", "MP3 files (*.mp3)"]
            folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
            fileMode: FileDialog.OpenFiles
            onAccepted: {
                for (var i=0; i<trackDialog.currentFiles.length; ++i) {
                    trackListModel.append({ name: "", track: ""+trackDialog.currentFiles[i], volume: 1.0 });
                }
                listChangesDone();
            }
        }

        // Eat all back button presses/releases to not exit so easily on android
        Keys.onPressed: (event)=> {
            if (event.key === Qt.Key_Back) {
                hSpeak.editMode = false;
                tracksListView.currentIndex = -1;
                event.accepted = true;
            }
        }
        Keys.onReleased: (event)=> {
            if (event.key === Qt.Key_Back) {
                hSpeak.editMode = false;
                tracksListView.currentIndex = -1;
                event.accepted = true;
            }
        }
    }

    Root.LicenseDialog {
        id: licenseDialog
        anchors.fill: parent
        property bool showOnStart: true
        visible: showOnStart
        onDismissed: showOnStart = false;
    }


    ListModel {
        id: soundListModel
        function setDefaults() {
            soundListModel.append({ name: "Buzzer", track: "qrc:/Buzzer.ogg", volume: 1.0 });
            soundListModel.append({ name: "Goal Horn", track: "qrc:/Horn.ogg", volume: 1.0 });
            soundListModel.append({ name: "Goal Cheer", track: "qrc:/GoalCheer.ogg", volume: 1.0 });
            soundListModel.append({ name: "Trumpets", track: "qrc:/Trumpets.ogg", volume: 1.0 });
            soundListModel.append({ name: "Build Cheer", track: "qrc:/Strong-building-team-cheer.ogg", volume: 1.0 });
            soundListModel.append({ name: "Final Fanfare", track: "qrc:/Fine.ogg", volume: 1.0 });
        }
    }

    ListModel {
        id: trackListModel
        function setDefaults() {
            trackListModel.append({ name: "Build Cheer", track: "qrc:/Strong-building-team-cheer.ogg", volume: 1.0 });
        }
    }

    Settings {
        property alias soundItems: hSpeak.soundList
        property alias trackItems: hSpeak.trackList
        property alias periodDuration: periodicItem.periodDuration
        property alias intervalDuration: periodicItem.intervalDuration
        property alias intervalTrackUrl: periodicItem.intervalTrackUrl
        property alias showLicenseInfo: licenseDialog.showOnStart
    }

    Component.onCompleted: {
        let periodicIntervalUrl = periodicItem.intervalTrackUrl.toString()
        soundListModel.clear();
        if (soundList !== "") {
            var items = JSON.parse(soundList);
            for (var i=0; i<items.length; ++i) {
                soundListModel.append(items[i]);
            }
        }
        if (soundListModel.count === 0) {
            soundListModel.setDefaults();
        }
        intervalCombo.currentIndex = intervalCombo.trackToIndex(periodicIntervalUrl);


        trackListModel.clear();
        if (trackList !== "") {
            var items = JSON.parse(trackList);
            for (var i=0; i<items.length; ++i) {
                trackListModel.append(items[i]);
            }
        }
        if (trackListModel.count === 0) {
            trackListModel.setDefaults();
        }
    }

    function listChangesDone() {
        var newSoundList = [];
        for (var i = 0; i < soundListModel.count; ++i) {
            newSoundList.push(soundListModel.get(i));
        }
        soundList = JSON.stringify(newSoundList);

        var newTraclList = [];
        for (var i = 0; i < trackListModel.count; ++i) {
            newTraclList.push(trackListModel.get(i));
        }
        trackList = JSON.stringify(newTraclList);
    }
}
