/*
 * SPDX-FileCopyrightText: 2020 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material
import sars.MobileHelpers
import "./" as Root

FocusScope {
    id: styledInput
    property alias text: input.text
    property alias color: input.color
    property alias font: input.font
    property alias validator: input.validator
    property string placeholderText: ""

    implicitWidth: styleRectangle.implicitWidth
    implicitHeight: styleRectangle.implicitHeight
    focus: true

    MobileHelpers { id: helpers }
    Material.theme: helpers.systemIsDarkMode ? Material.Dark : Material.Light

    Rectangle {
        id: styleRectangle
        width: parent.width
        height: parent.height
        implicitWidth: input.implicitWidth
        implicitHeight: input.implicitHeight + pageMargins * 2

        border.color: input.activeFocus ? "#88BBFF" : "gray"
        border.width: 1
        color: "#22FFFFFF"
        radius: pageMargins / 4

        TextInput {
            id: input
            anchors.centerIn: parent
            width: parent.width - pageMargins * 2
            clip: true
            color: helpers.systemIsDarkMode ? "white" : "black"
            focus: true

            Label {
                anchors.fill: parent
                text: styledInput.placeholderText
                color: helpers.systemIsDarkMode ? "#88FFFFFF" : "#55000000"
                visible: input.displayText == ""
            }

        }
    }
}
