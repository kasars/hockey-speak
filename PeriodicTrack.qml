/*
 * SPDX-FileCopyrightText: 2020-2023 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtMultimedia
import QtQuick.Controls.Material
import sars.MobileHelpers
import "./" as Root

Item {
    id: periodicTrack
    property string intervalTrackUrl: "qrc:/Buzzer.ogg"
    property alias endTrackUrl: endTrack.source
    property int intervalDuration: 113
    property int periodDuration: 900

    property bool pause: false

    property bool portrait: false

    property double periodPlayed: 0
    property double periodEndStamp: Date.now() + periodInput.time_ms
    property bool playing: false;
    readonly property double intervalPlayed: periodPlayed % intervalInput.time_ms

    MobileHelpers { id: helpers }
    Material.theme: helpers.systemIsDarkMode ? Material.Dark : Material.Light

    Rectangle {
        id: bgItem
        anchors.fill: parent
        color: helpers.systemIsDarkMode ? "#464646" : "#f5f5f5"
        opacity: 0.7
        Rectangle {
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            height: 1
            color: "gray"
        }
        Rectangle {
            anchors {
                bottom: parent.bottom
                left: parent.left
                right: parent.right
            }
            height: 1
            color: "gray"
        }
    }

    readonly property int buttonsHeight: Math.max(inputRow.implicitHeight, buttonColumn.implicitHeight) + pageMargins * 2

    implicitHeight: portrait ? progressColumn.implicitHeight + buttonsHeight + pageMargins : buttonsHeight
    implicitWidth: inputRow.implicitWidth + pageMargins * 4 + progressColumn.implicitWidth + buttonColumn.implicitWidth

    Row {
        id: inputRow
        anchors {
            top: portrait ? progressColumn.bottom : parent.top
            left: parent.left
            bottom: parent.bottom
            margins: pageMargins
        }
        spacing: pageMargins * 2
        MinSecInput {
            id: periodInput
            anchors.bottom: parent.bottom
            label: qsTr("Period")
            minutes: periodDuration / 60
            seconds: periodDuration % 60
            onTimeEdited: { periodDuration = time; }
            disabled: playing
        }
        MinSecInput {
            id: intervalInput
            label: qsTr("Sound Interval")
            anchors.bottom: parent.bottom
            minutes: intervalDuration / 60
            seconds: intervalDuration % 60
            onTimeEdited: intervalDuration = time;
            disabled: playing
        }
    }

    function minSecsFromMsec(msecs) {
        let date = new Date(msecs+100); // 100 ms extra for rounding
        if (msecs > 3600000) {
            return Qt.formatTime(date, "hh:mm:ss");
        }
        return Qt.formatTime(date, "mm:ss");
    }

    Item {
        id: layoutHelper
        anchors.top: parent.top
        width: 1
        height: parent.height - progressColumn.height - pageMargins * 2
        // NOTE: This is a bit ugly workaround for a binding execution order problem
        // binding top or bottom depending on portrait resulted in intermediately anchoring
        // both top and bottom, which modified the size of progressColumn
    }

    Column {
        id: progressColumn
        anchors {
            top:  portrait ? parent.top : layoutHelper.bottom
            left: portrait ? parent.left : inputRow.right
            right: portrait ? parent.right : buttonColumn.left
            margins: pageMargins
        }

        spacing: pageMargins

        Root.ProgressBar {
            width: progressColumn.width
            height: startBtn.height
            progress: periodPlayed / (periodInput.time_ms)
            clip: true
            leftText: qsTr("Period")
            rightText: qsTr("%1 / %2").arg(minSecsFromMsec(periodPlayed)).arg(minSecsFromMsec(periodInput.time_ms))
            backgroundColor: helpers.systemIsDarkMode ? "#555555" : "#DDDDDD"
            textColor: helpers.systemIsDarkMode ? "white" : "black"
            onDragToPositionChanged: {
                if (playing) {
                    pauseInfoTimer.start();
                    return;
                }
                periodPlayed = dragToPosition * periodInput.time_ms;
                periodEndStamp = Date.now() + periodInput.time_ms - periodPlayed;
            }
        }
        Root.ProgressBar {
            width: progressColumn.width
            height: startBtn.height
            progress: intervalPlayed / intervalInput.time_ms
            clip: true
            leftText: qsTr("Interval (%1 of %2)")
            .arg(Math.floor(periodPlayed / intervalInput.time_ms)+1)
            .arg((periodInput.time / intervalInput.time).toFixed(2))
            rightText: qsTr("%1 / %2").arg(minSecsFromMsec(intervalPlayed)).arg(minSecsFromMsec(intervalInput.time_ms))
            backgroundColor: helpers.systemIsDarkMode ? "#555555" : "#DDDDDD"
            textColor: helpers.systemIsDarkMode ? "white" : "black"
            onDragToPositionChanged: {
                if (playing) {
                    pauseInfoTimer.start();
                    return;
                }
                const interval = Math.floor(periodPlayed / intervalInput.time_ms);
                periodPlayed = interval * intervalInput.time_ms + dragToPosition * intervalInput.time_ms;
                periodEndStamp = Date.now() + periodInput.time_ms - periodPlayed;
            }
        }
    }

    Timer {
        id: pauseInfoTimer
        interval: 1500
    }

    Rectangle {
        anchors.fill: progressColumn
        border.width: 1
        border.color: "gray"
        color: helpers.systemIsDarkMode ? "#555555" : "#DDDDDD"
        radius: pageMargins / 4
        opacity: 0.7
        visible: pauseInfoTimer.running
    }
    Text {
        anchors.centerIn: progressColumn
        text: qsTr("Pause to edit played time")
        visible: pauseInfoTimer.running
        color: helpers.systemIsDarkMode ? "white" : "black"
    }

    Column {
        id: buttonColumn
        anchors {
            //top: parent.top
            right: parent.right
            bottom: parent.bottom
            margins: pageMargins
        }
        spacing: pageMargins

        Root.Button {
            id: startBtn
            width: Math.max(startBtn.implicitWidth, pauseBtn.implicitWidth, stopBtn.implicitWidth)
            text: qsTr("Start");
            onClicked: {
                periodEndStamp = Date.now() + periodInput.time_ms - periodPlayed;
                playing = true;
            }
            visible: !playing
        }
        Root.Button {
            id: pauseBtn
            width: startBtn.width
            text: qsTr("Pause");
            onClicked: {
                playing = false;
            }
            visible: playing
        }
        Root.Button {
            id: stopBtn
            width: startBtn.width
            text: qsTr("Reset");
            onClicked: {
                playing = false;
                periodPlayed = 0;
            }
            disabled: periodPlayed === 0 || playing
        }
    }

    Timer {
        id: updateTime
        interval: 50
        repeat: true
        running: playing
        onTriggered: {
            const now = Date.now();
            periodPlayed = (periodInput.time_ms) - (periodEndStamp - now);

            if (periodPlayed > periodInput.time_ms) {
                playing = false;
                periodPlayed = 0;
                endTrack.play();
            }

            if (intervalPlayed > (intervalInput.time_ms-100)) {
                intervalTrack.play();
            }
        }
    }

    MediaPlayer {
        id: intervalTrack
        audioOutput: AudioOutput {}
        source: periodicTrack.intervalTrackUrl

        onMediaStatusChanged: {
            if (mediaStatus === MediaPlayer.LoadedMedia) {
                intervalTrack.pause();
                intervalTrack.setPosition(0);
            }
        }
    }
    MediaPlayer {
        id: endTrack
        audioOutput: AudioOutput {}
        source: "qrc:/Horn.ogg"

        onMediaStatusChanged: {
            if (mediaStatus === MediaPlayer.LoadedMedia) {
                endTrack.pause();
                endTrack.setPosition(0);
            }
        }
    }
}
