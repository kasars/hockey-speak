/*
 * SPDX-FileCopyrightText: 2020-2023 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtMultimedia
import QtQuick.Layouts
import QtQuick.Controls as QC
import QtQuick.Controls.Material
import sars.MobileHelpers
import "./" as Root

Item {
    id: spItem

    property string trackName: ""
    property string soundSource: ""
    property double soundVolume: 1.0
    readonly property bool playing: soundTrack.playbackState === MediaPlayer.PlayingState
    property bool mediaLoaded: false

    property bool editMode: false

    onEditModeChanged: {
        if (!editMode) {
            input.focus = false;
        }
    }

    MobileHelpers { id: helpers }
    Material.theme: helpers.systemIsDarkMode ? Material.Dark : Material.Light

    implicitHeight: playButton.implicitHeight

    signal editClicked()
    signal removeClicked()
    signal endReached()
    signal clicked()

    signal labelChanged(string newText);

    function play() {
        soundTrack.play();
    }

    TextMetrics {
        id: volumeTextMetrics
        text: qsTr("Vol: %1%").arg(100)
    }


    Row {
        id: buttonRow
        anchors.fill: parent
        spacing: pageMargins * 0.5

        Item {
            id: playPauseItem
            height: spItem.height
            width: spItem.height
            visible: !editMode
            Root.IconLabel {
                id: playButton
                height: spItem.height
                width: height
                icon: "Play.png"
                visible: !pauseButton.visible
                onClicked: {
                    soundTrack.play();
                    spItem.clicked();
                }
            }
            Root.IconLabel {
                id: pauseButton
                height: spItem.height
                width: height
                visible: soundTrack.playbackState === MediaPlayer.PlayingState
                icon: "Pause.png"
                onClicked: {
                    soundTrack.pause();
                    spItem.clicked();
                }
            }
        }

        Root.IconLabel {
            id: volumeButton
            height: spItem.height
            width: height
            icon: helpers.systemIsDarkMode ? "SpeakerDM.png" : "Speaker.png"
            visible: editMode
            checkable: true
            onClicked: {
                spItem.clicked();
            }
        }

        RowLayout {
            id: volRow
            width: parent.width - playPauseItem.width - pageMargins - removeButton.width
            height: parent.height
            QC.Label {
                Layout.minimumWidth: volumeTextMetrics.width
                property int volume: (volumeSlider.value * 100.0)
                text: qsTr("Vol: %1%").arg(volume)
                color: helpers.systemIsDarkMode ? "white" : "black"
            }

            QC.Slider {
                id: volumeSlider
                Layout.fillWidth: true
                Layout.fillHeight: true
                from: 0.0
                value: spItem.soundVolume
                to: 1.0
            }

            Binding {
                target: spItem
                property: "soundVolume"
                value: volumeSlider.value
            }
            visible: editMode && volumeButton.checked
        }

        Root.ProgressBar {
            id: playbackInfo
            width: parent.width - playPauseItem.width - pageMargins * 0.5
            height: parent.height

            progress: soundTrack.playbackState === MediaPlayer.StoppedState ? 0 : (soundTrack.position / soundTrack.duration)
            property string playMarginStr: pauseButton.visible ? "\u2003\u2003\u2002" : "" //
            property string mediaTitle: strValue(soundTrack.metaData, "Title")
            property string mediaArtist: strValue(soundTrack.metaData, "Contributing artist")
            property string fileBaseName: fileName(soundSource)
            property string mediaLabel: trackName !== "" ? trackName : mediaArtist !== "" ? mediaArtist +  " - " + mediaTitle :  mediaTitle !== "" ? mediaTitle : fileBaseName
            leftText: playMarginStr + mediaLabel
            rightText: qsTr("%1 / %2").arg(minSecsFromMsec(soundTrack.position)).arg(minSecsFromMsec(soundTrack.duration))

            backgroundColor: helpers.systemIsDarkMode ? "#555555" : "#DDDDDD"
            textColor: helpers.systemIsDarkMode ? "white" : "black"

            function minSecsFromMsec(msecs) {
                let date = new Date(msecs);
                if (msecs > 3600000) {
                    return Qt.formatTime(date, "hh:mm:ss");
                }
                return Qt.formatTime(date, "mm:ss");
            }

            onDragToPositionChanged:  {
                soundTrack.setPosition(dragToPosition * soundTrack.duration);
                spItem.clicked();
            }

            Image {
                anchors {
                    left: parent.left
                    verticalCenter: parent.verticalCenter
                }
                height: parent.height * 0.7
                width: height
                source: helpers.systemIsDarkMode ? "PlayIndicatorDM.png" : "PlayIndicator.png"
                visible: pauseButton.visible
            }
            visible: !editMode

            progressColor: pauseButton.visible ? "#350080ff" : "#200080ff"
        }

        Root.StyledInput {
            id: input
            width: parent.width - playPauseItem.width - pageMargins - removeButton.width
            height: parent.height
            text: spItem.trackName
            placeholderText: playbackInfo.mediaLabel
            visible: spItem.editMode && !volRow.visible
            Rectangle {
                anchors {
                    left: parent.left
                    bottom: parent.bottom
                }
                z: -1
                color: "#500080ff"
                height: 4
                width: parent.width * soundVolume
            }
        }

        Binding {
            target: spItem
            property: "trackName"
            value: input.text
        }


        Root.IconLabel {
            id: removeButton
            height: spItem.height
            width: height
            icon: helpers.systemIsDarkMode ? "QuitButtonDM.png" : "QuitButton.png"
            onClicked: spItem.removeClicked();
            visible: editMode
        }
    }

    MediaPlayer {
        id: soundTrack
        source: soundSource
        audioOutput: AudioOutput { volume: Math.max(0, Math.min(volumeSlider.value, 1.0)); }

        onErrorOccurred: { console.log("errorString:", soundTrack.errorString) }

        onPlaybackStateChanged: {
            if (soundTrack.playbackState ===  MediaPlayer.StoppedState) {
                soundTrack.setPosition(0);
                spItem.endReached();
            }
        }

        onMediaStatusChanged: {
            if (mediaStatus === MediaPlayer.LoadedMedia) {
                soundTrack.pause();
                soundTrack.setPosition(0);
                mediaLoaded = true;
            }
        }
    }

    function strValue(metadata: MediaMetaData, strKey: string) {
        for (var key of metadata.keys()) {
            if (metadata.metaDataKeyToString(key) === strKey) {
                return metadata.stringValue(key);
            }
        }
        return "";
    }

    function fileName(url: string) {
        const lastFolder = url.lastIndexOf("/") + 1;
        const androidUrl = url.lastIndexOf("%2F") + 3;
        return url.slice(Math.max(lastFolder, androidUrl));
    }

    Rectangle {
        id: loadingLabel
        anchors.fill: parent
        color: helpers.systemIsDarkMode ? "#555555" : "#DDDDDD"
        Text {
            anchors.centerIn: parent
            text: qsTr("Loading...")
            color: helpers.systemIsDarkMode ? "#ffffff" : "#000000"
        }
        opacity: 0.9
        visible: soundTrack.mediaStatus === MediaPlayer.LoadingMedia
    }
}
