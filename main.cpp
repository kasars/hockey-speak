/*
 * SPDX-FileCopyrightText: 2020 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#include "MobileHelpers.h"
#include <QtGlobal>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

#ifdef Q_OS_ANDROID
#include <QGuiApplication>
#define HSApplication QGuiApplication
#else
#include <QApplication>
#define HSApplication QApplication
#endif

int main(int argc, char *argv[])
{
    HSApplication app(argc, argv);
    app.setOrganizationName("Sars Productions");
    app.setApplicationName("Hockey Speak");

    qmlRegisterType<MobileHelpers>("sars.MobileHelpers", 1, 0, "MobileHelpers");

    MobileHelpers::setApplicationFontSize(14);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    MobileHelpers::keepScreenOn(true);

    return app.exec();
}
