#!/bin/bash

# mkdir build_android
# cd build_android
# ../configure_android_qt6.sh

BUILD_TYPE=$1
if [[  "$BUILD_TYPE" == ""  ]]; then
    # By default build Debug
    # NOTE: Release builds must be signed to be installed
    BUILD_TYPE="Debug"
fi

export ANDROID_API=33
export ANDROID_TARGET_SDK_VERSION=33
export ANDROID_SDK_ROOT=/opt/Android/android-sdk
export ANDROID_NDK_ROOT=/opt/Android/android-sdk/ndk/25.2.9519653


/opt/Qt/6.6.1/android_x86_64/bin/qt-cmake \
    -G Ninja \
    -DCMAKE_BUILD_TYPE=$BUILD_TYPE \
    -DQT_ANDROID_BUILD_ALL_ABIS=ON \
    -S ../ -B ./


# Build the package
#ninja


# Instal to the phone
#adb install -r android-build/build/outputs/apk/debug/android-build-debug.apk


# Start for some reason we need the sleep here
#sleep 1
#adb shell am start -S -W -n org.sars.hockeyspeak/org.qtproject.qt.android.bindings.QtActivity

# sign the bundle
# jarsigner --verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore <key> -signedjar <out.aab> android-build/build/outputs/bundle/release/android-build-release.aab "<cert alias>"

