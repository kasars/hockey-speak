/*
 * SPDX-FileCopyrightText: 2020 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtQuick.Controls as QC
import QtQuick.Layouts

FocusScope {
    property alias name: nameInput.text
    property alias volume: volumeSlider.value
    implicitHeight: contentColumn.implicitHeight
    implicitWidth: contentColumn.implicitWidth

    Column {
        id: contentColumn
        anchors.fill: parent

        spacing: pageMargins
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Track Label:")
        }

        StyledInput {
            id: nameInput
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
        }

        Item {  width: 1; height: 1; }

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Track Volume:")
        }

        RowLayout {
            width: parent.width
            Text {
                text: qsTr("0%")
            }

            QC.Slider {
                Layout.fillWidth: true
                Layout.fillHeight: false

                id: volumeSlider
                from: 0.0
                value: 1.0
                to: 1.0
            }

            Text {
                text: qsTr("100%")
            }

        }


    }
}
