/*
 * SPDX-FileCopyrightText: 2020 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtQuick.Controls

Item {
    id: iconLabel

    property alias icon: iconItem.source
    property alias text: textItem.text
    property bool disabled: false

    property bool checkable: false
    readonly property bool checked: mArea.checked;

    height: textItem.font.pixelSize * 2
    width: iconItem.width + (textItem.text !== "" ? textItem.width + pageMargins : 0)


    signal clicked()

    Rectangle {
        anchors.fill: iconItem
        color: "#300080ff"
        visible: iconLabel.checked
        radius: width / 3
    }

    Image {
        id: iconItem
        anchors {
            top: parent.top
            left: parent.left
            bottom: parent.bottom
        }
        width: height
        sourceSize.width: width
        sourceSize.height: height
        smooth: true
    }

    Label {
        id: textItem
        anchors {
            top: parent.top
            left: iconItem.right
            bottom: parent.bottom
            leftMargin: pageMargins
        }
        font.bold: true
        color: "black"
        style: Text.Outline;
        styleColor: "white"
        verticalAlignment: Text.AlignVCenter
        visible: text !== ""
    }

    function toggleChecked() { mArea.checked = !mArea.checked; }

    opacity: disabled ? 0.5 : 1.0
    MouseArea {
        id: mArea
        property bool checked: false
        anchors.fill: parent
        onClicked: {
            if (iconLabel.checkable) {
                checked = !checked;
            }
            iconLabel.clicked();
        }

        visible: !disabled
    }
}
