/*
 * SPDX-FileCopyrightText: 2020-2024 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material
import sars.MobileHelpers

Item {
    id: button

    signal clicked()

    property alias text: textItem.text
    property alias font: textItem.font
    property bool disabled: false
    property bool darkMode: false

    property alias pressed: mArea.pressed

    property double progress: 0 ///< 0.0-1.0

    implicitWidth: Math.round(textItem.implicitWidth + textItem.font.pixelSize * 3)
    implicitHeight: Math.round(textItem.font.pixelSize + pageMargins * 2)

    MobileHelpers { id: helpers }
    Material.theme: helpers.systemIsDarkMode ? Material.Dark : Material.Light

    Rectangle {
        id: background
        anchors.fill: parent
        border.width: 1
        border.color: "gray"
        radius: pageMargins / 4
        readonly property color bgColor: helpers.systemIsDarkMode ? "#555555" : "#f0f1f2"
        gradient: Gradient {
            GradientStop { position: 0.0; color: background.bgColor }
            GradientStop { position: 0.66; color: Qt.darker(background.bgColor, 1.05)}
            GradientStop { position: 1.0; color: Qt.darker(background.bgColor, 1.1) }
        }
        opacity: 0.8
    }

    Rectangle {
        id: progressItem
        anchors {
            top: background.top
            left: background.left
            bottom: background.bottom
            margins: background.border.width
        }
        property double displayProgress: Math.min(1.0, progress)
        radius: background.radius
        width: (background.width - 2 * background.border.width) * displayProgress
        color: "#200080ff"
        Rectangle {
            anchors {
                top: parent.top
                right: parent.right
                bottom: parent.bottom
            }
            width: 1
            color: "#400080ff"
            visible: (progressItem.displayProgress * background.width > 0.5) &&
                    ((background.width - (progressItem.displayProgress * background.width)) > 0.5)
        }
    }

    Rectangle {
        anchors.fill: parent
        anchors.margins: background.border.width
        color: "lightGray"
        radius: background.radius
        opacity: 0.6
        visible: mArea.pressed
    }

    Label {
        id: textItem
        anchors.centerIn: parent
        opacity: disabled ? 0.7 : 1
    }

    Rectangle {
        id: disabledIndicator
        anchors.fill: parent
        anchors.margins: background.border.width
        color: helpers.systemIsDarkMode ? "#757575" : "lightGray"
        radius: background.radius
        opacity: 0.5
        visible: disabled
    }

    MouseArea {
        id: mArea
        anchors.fill: parent
        onClicked: button.clicked()
        visible: !disabled
    }

}
