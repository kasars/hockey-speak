/*
 * SPDX-FileCopyrightText: 2020 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtQuick.Controls.Material
import sars.MobileHelpers

Column {
    id: minSecEdit
    property alias label: labelItem.text
    property alias labelFont: labelItem.font

    property alias minutes: minInput.value
    property alias seconds: secInput.value

    property bool disabled: false

    readonly property double time: minutes * 60 + seconds
    readonly property double time_ms: time * 1000

    signal timeEdited()

    spacing: pageMargins
    MobileHelpers { id: helpers }
    Material.theme: helpers.systemIsDarkMode ? Material.Dark : Material.Light


    Text {
        id: labelItem
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.Wrap
        visible: text !== ""
        color: helpers.systemIsDarkMode ? "white" : "black"
    }

    Row {
        id: inputRow
        spacing: pageMargins/4
        NumberInput {
            id: minInput
            onValueEdited: minSecEdit.timeEdited();
            disabled: minSecEdit.disabled
        }
        Text { anchors.verticalCenter: parent.verticalCenter; text: ":";  color: labelItem.color }
        NumberInput {
            id: secInput
            maxVal: 59
            disabled: minSecEdit.disabled
            onIncrementMax: {
                value = 0;
                minInput.increment();
            }
            onDecrementMin: {
                if (minInput.value > 0) {
                    minInput.decrement();
                    value = 59;
                }
            }
            onValueEdited: minSecEdit.timeEdited();
        }
    }
}
