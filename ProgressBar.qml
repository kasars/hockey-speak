/*
 * SPDX-FileCopyrightText: 2020.2021 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick

Item {
    id: progressBar

    property alias leftText: leftTextItem.text
    property alias rightText: rightTextItem.text
    property alias progressColor: progressRect.color
    property double progress: 0 // 0.00 - 1.00
    property double dragToPosition: 0
    property color backgroundColor: "#DDDDDD"
    property color textColor: "black"

    Rectangle {
        id: background
        anchors.fill: parent
        color: progressBar.backgroundColor
        opacity: 0.7
    }

    Rectangle {
        id: progressRect
        anchors {
            top: parent.top
            left: parent.left
            bottom: parent.bottom
        }
        color: "#300080ff"
        width: parent.width * progress
    }

    Text {
        id: leftTextItem
        anchors {
            top: parent.top
            left: parent.left
            right: rightTextItem.left
            rightMargin: pageMargins
            leftMargin: pageMargins
            bottom: parent.bottom
        }
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
        //font.bold: false
        color: progressBar.textColor
    }
    Text {
        id: rightTextItem
        anchors {
            top: parent.top
            right: parent.right
            rightMargin: pageMargins
            bottom: parent.bottom
        }
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideLeft
        //font.bold: false
        color: progressBar.textColor
    }
    MultiPointTouchArea {
        anchors.fill: parent
        minimumTouchPoints: 1
        maximumTouchPoints: 1

        touchPoints: [
        TouchPoint {
            id: touch1
            property real lastX: -1
            onPressedChanged: { if (pressed) { lastX = -1; } }
        }
        ]

        onTouchUpdated: {
            if (touch1.lastX == -1) touch1.lastX = touch1.x;
            var newX = progressRect.width - (touch1.lastX - touch1.x);
            newX = Math.min(Math.max(0, newX), progressBar.width);
            touch1.lastX = touch1.x;
            dragToPosition = Math.min(0.9999, newX/progressBar.width);
        }
    }
}
