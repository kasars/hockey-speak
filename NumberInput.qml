/*
 * SPDX-FileCopyrightText: 2020 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtQuick.Controls.Material
import sars.MobileHelpers
import "./" as Root

Item {
    id: numberInput
    implicitWidth: Math.max(incrButton.implicitWidth, input.implicitWidth)
    implicitHeight: incrButton.implicitHeight + input.implicitHeight + decrButton.implicitHeight + pageMargins

    signal incrementMax()
    signal decrementMin()
    signal valueEdited()

    property int value: 0

    property int minVal: 0
    property int maxVal: 60
    property bool disabled: false

    MobileHelpers { id: helpers }
    Material.theme: helpers.systemIsDarkMode ? Material.Dark : Material.Light

    Rectangle {
        id: background
        anchors.fill: parent
        anchors.margins: 0
        color: helpers.systemIsDarkMode ? "#2b2b2b" : "#ffffff"
        radius: pageMargins / 4
        border.width: 1
        border.color: "#AFAFAF"
    }

    function prefixed(val) {
        if (val < 10) return "0" + val;
        return "" + val;
    }

    function increment() {
        if (numberInput.value < numberInput.maxVal) {
            numberInput.value++;
        }
        else {
            numberInput.incrementMax();
        }
        numberInput.valueEdited();
    }

    function decrement() {
        if (numberInput.value > numberInput.minVal) {
            numberInput.value--;
        }
        else {
            numberInput.decrementMin();
        }
        numberInput.valueEdited();
    }

    Timer {
        id: incTmrStart
        interval: 500
        running: incrButton.pressed
    }
    Timer {
        id: incTmr
        interval: 150
        repeat: true
        running: incrButton.pressed
        onTriggered: {
            if (incTmrStart.running) return;
            increment();
        }
    }

    Timer {
        id: decTmrStart
        interval: 500
        running: decrButton.pressed
    }
    Timer {
        id: decTmr
        interval: 150
        repeat: true
        running: decrButton.pressed
        onTriggered: {
            if (decTmrStart.running) return;
            decrement();
        }
    }

    Root.Button {
        id: incrButton
        anchors {
            top: parent.top
            left: parent.left
            right:parent.right
        }
        implicitWidth: Math.round(pageMargins * 4)
        implicitHeight: pageMargins * 3
        text: qsTr("+")
        onClicked: increment()
    }
    Text {
        id: input
        anchors.centerIn: parent
        focus: true
        color: helpers.systemIsDarkMode ? "white" : "black"
        text: prefixed(numberInput.value)
    }
    Root.Button {
        id: decrButton
        anchors {
            bottom: parent.bottom
            left: parent.left
            right:parent.right
        }
        implicitWidth: Math.round(pageMargins * 4)
        implicitHeight: pageMargins * 3
        text: qsTr("-")
        onClicked: decrement()
    }

    Rectangle {
        anchors.fill: parent
        anchors.margins: background.border.width
        color: helpers.systemIsDarkMode ? "#757575" : "lightGray"
        radius: background.radius
        opacity: 0.6
        visible: disabled
        MouseArea { anchors.fill: parent }
    }
}
