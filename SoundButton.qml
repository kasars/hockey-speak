/*
 * SPDX-FileCopyrightText: 2021-2023 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtMultimedia

Item {
    id: soundButton

    property alias trackName: button.text
    property alias soundSource: soundTrack.source
    property double soundVolume: 1.0
    property bool mediaLoaded: false
    property bool disabled: !mediaLoaded

    Button {
        id: button
        anchors.fill: parent
        progress: (soundTrack.position) / soundTrack.duration
        disabled: soundButton.disabled || !mediaLoaded
    }

    signal endReached()

    MediaPlayer {
        id: soundTrack
        audioOutput: AudioOutput { volume: Math.max(0, Math.min(soundButton.soundVolume, 1.0)); }
        onErrorOccurred: { console.log("errorString:", soundTrack.errorString, soundTrack.source) }

        onPlaybackStateChanged: {
            if (soundTrack.playbackState === MediaPlayer.StoppedState) {
                soundTrack.pause();
                soundTrack.setPosition(0);
                //soundButton.endReached();
            }
        }

        onMediaStatusChanged: {
            if (mediaStatus === MediaPlayer.LoadedMedia) {
                soundTrack.pause();
                soundTrack.setPosition(0);
                mediaLoaded = true;
            }
        }
    }

    MouseArea {
        id: mArea
        anchors.fill: parent
        onClicked: {
            if (soundTrack.playbackState === MediaPlayer.PlayingState) {
                soundTrack.stop();
            }
            else {
                soundTrack.play();
            }
        }
        visible: !button.disabled
    }

}
